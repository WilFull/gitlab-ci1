from temp import convert

def test1():
	assert convert(1.0, 'C', 'F') == 33.8

def test2():
	assert convert(33.8, 'F', 'K') == 274.15

def test3():
	assert convert(274.15, 'K', 'C') == 1.0

def test4():
	assert convert(33.8, 'F', 'C') == 1.0
